#pragma once
#include "CoreMinimal.h"
#include "Subsystems/GameInstanceSubsystem.h"
#include "MyGameInstanceSubsystemCpp.generated.h"

UCLASS(DisplayName = "CppSubsystem", Blueprintable)
class SUBSYSTEMTEST_API UMyGameInstanceSubsystemCpp : public UGameInstanceSubsystem
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "MyCategory")
	FString GetMyString() const;
};
