// Copyright Epic Games, Inc. All Rights Reserved.

#include "SubsystemTest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SubsystemTest, "SubsystemTest" );
