// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SubsystemTestGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SUBSYSTEMTEST_API ASubsystemTestGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
